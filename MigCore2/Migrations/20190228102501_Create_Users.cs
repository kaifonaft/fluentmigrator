﻿using FluentMigrator;

namespace MigCore2.Migrations
{
    [Migration(20190228102501)]
    public class Create_Users : Migration
    {
        private readonly string tableName = "Users";
        public override void Up()
        {
            Create.Table(tableName)
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("name").AsString().NotNullable();
            //Create.ForeignKey()
            //    .FromTable(tableName).ForeignColumn("company_id")
            //    .ToTable("Company").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table(tableName);
        }
    }
}
