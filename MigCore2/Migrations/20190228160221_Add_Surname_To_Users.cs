﻿using FluentMigrator;

namespace MigCore2.Migrations
{
    [Migration(20190228160221)]
    public class Add_Surname_To_Users : Migration
    {
        private string tableName = "users";
        private string columnName = "surname";
        public override void Up()
        {
            Alter.Table(tableName)
                .AddColumn(columnName)
                .AsString(255).Nullable();
        }

        public override void Down()
        {
            Delete.Column(columnName).FromTable(tableName);
        }
    }
}
