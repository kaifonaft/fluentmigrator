﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace ConsoleApp1.Migrations
{
    [Migration(20190228102501)]
    public class Create_Users : Migration
    {
        private readonly string tableName = "Users";
        public override void Up()
        {
            Create.Table(tableName)
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("Name").AsString().NotNullable();
            //Create.ForeignKey()
            //    .FromTable(tableName).ForeignColumn("company_id")
            //    .ToTable("Company").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table(tableName);
        }
    }
}
