﻿using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;
using System.Data.SQLite;
using FluentMigrator.Runner.Initialization;
using MigCore2.Migrations;

namespace MigCore2
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = CreateServices();

            // Put the database update into a scope to ensure
            // that all resources will be disposed.
            using (var scope = serviceProvider.CreateScope())
            {
                UpdateDatabase(scope.ServiceProvider);
            }
        }

        /// <summary>
        /// Configure the dependency injection services
        /// </sumamry>
        private static IServiceProvider CreateServices()
        {
            return new ServiceCollection()
                // Add common FluentMigrator services

                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    //// Add SQLite support to FluentMigrator
                    //.AddSQLite()
                    //// Set the connection string
                    //.WithGlobalConnectionString("Data Source=../../../blog.sqlite")

                    // Add MySQL support to FluentMigrator
                    .AddMySql5()
                    // Set the connection string
                    .WithGlobalConnectionString("Data Source=localhost; Database=blog; User Id=root; Password=root; SslMode=none")
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(Create_Users).Assembly)
                        .For.Migrations()
                        .For.EmbeddedResources()
                )
                // Enable logging to console in the FluentMigrator way
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                // Build the service provider
                .BuildServiceProvider(false);
        }

        /// <summary>
        /// Update the database
        /// </sumamry>
        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateUp();
            
            //// откатить все миграции
            //runner.MigrateDown(0);
        }
    }
}
