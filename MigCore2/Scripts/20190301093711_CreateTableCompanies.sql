﻿USE blog;
CREATE TABLE companies
		(
		id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
		name varchar(255) NOT NULL,
		contact_id int,
		CONSTRAINT companies_users_id_fk FOREIGN KEY (contact_id) REFERENCES users (id)
		);