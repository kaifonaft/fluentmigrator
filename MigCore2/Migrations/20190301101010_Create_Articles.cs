﻿using FluentMigrator;

namespace MigCore2.Migrations
{
    [Migration(20190301101010)]
    public class Create_Articles : Migration
    {
        private readonly string tableName = "articles";
        public override void Up()
        {
            Create.Table(tableName)
                .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("author_id").AsInt32()
                .WithColumn("title").AsString(255).NotNullable()
                .WithColumn("description").AsString(257)
                .WithColumn("text").AsCustom("TEXT");
            Create.ForeignKey()
                .FromTable(tableName).ForeignColumn("author_id")
                .ToTable("users").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.Table(tableName);
        }
    }
}
