﻿using FluentMigrator;

namespace MigCore2.Migrations
{
    [Migration(20190301093711)]
    public class Create_Table_Companies : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("20190301093711_CreateTableCompanies.sql");
        }

        public override void Down()
        {
            Execute.EmbeddedScript("20190301093711_DropTableCompanies.sql");
        }
    }
}
