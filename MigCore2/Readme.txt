﻿Создайте локальную базу(MySQL) blog. На MariaDb - вроде канает.
Запустите проект MigCore2. Применятся все миграции

Для работы с миграциями через консоль, запускаем командную строку внутри папки проекта MigCore2(запускаем 2.bat)
И применяем вот эту замечательную строчку с "параметрами консоли":
..\packages\FluentMigrator.Console.3.1.3\net461\x64\Migrate.exe /connection "data source=localhost;initial catalog=blog; User ID=root;Password=root; SslMode=none" /db MySQL /timeout 600 /target ..\MigCore2\bin\Debug\netcoreapp2.2\MigCore2.dll
либо вот так:
fm.bat -t=migrate

/target - dll-ка в которой содержатся все классы миграций и SQL-ки, установленные как Embedd Resource
/connection - строка подключения к базе

Инфа о "параметрах консоли" здесь:
https://fluentmigrator.github.io/articles/runners/runner-console.html?q=console%20rollback

	Применить все миграции:
	-t migrate
	--task=migrate:up

	Откатить одну миграцию
	-t=rollback
	
	Откатить к заданой версии:
	-t=rollback:toversion --version=20190228160221

Для запуска только через core, без framework пакетов(типа вот этих: packages\FluentMigrator.Console.3.1.3\net461\x64\Migrate.exe) нужно поставить вот эту штуку:	
https://fluentmigrator.github.io/articles/runners/dotnet-fm.html
выполняем эту строку в консоли:
dotnet tool install -g FluentMigrator.DotNet.Cli
И потом можно с ней работать через команду:
dotnet-fm 
dotnet-fm migrate -p=MySql -a="bin\Debug\netcoreapp2.2\MigCore2.dll" -c="data source=localhost;initial catalog=blog; User ID=root;Password=root; SslMode=none"
dotnet-fm rollback -p=MySql -a="bin\Debug\netcoreapp2.2\MigCore2.dll" -c="data source=localhost;initial catalog=blog; User ID=root;Password=root; SslMode=none"
dotnet-fm rollback -p=MySql -a="bin\Debug\netcoreapp2.2\MigCore2.dll" -c="data source=localhost;initial catalog=blog; User ID=root;Password=root; SslMode=none" to 20190228160221
-a, --assembly=<dll-path> - путь к dll-ке в которой содержатся все классы миграций и SQL-ки, установленные как Embedd Resource
-c, --connection=<connection-string> - строка подключения к базе
либо так(через батник проекта MigCore2):
Применить все миграции:
	fmcore.bat migrate
Откатить одну миграцию
	fmcore.bat rollback
Откатить к заданой версии:
	fmcore.bat rollback to 20190228160221